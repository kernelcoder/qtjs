
function HBox (parent) {
    this.spacing = 10;
    this.parent = parent;
    this.children = []
    var This = this;
    parent.resize(function () {
        This.arrangeChildren();
    });
}


HBox.prototype.addChild = function(child, options) {
    console.log('adding child ' + child)
    var This = this;
    var opt =  {'hoffset': 0, 'voffset': 0};
    jQuery.extend(opt, options);
    This.children.push({'child': child, 'options': opt });
    child.resize(function () {
        This.arrangeChildren();
    });
    child.css({position: 'absolute'});
};
 
 
HBox.prototype.arrangeChildren = function() {
    var This = this;
    var totalWidth = 0;    

    jQuery.each(This.children, function (k,v) {
        totalWidth += $(v.child).width();
    });
    
    var x = ($(This.parent).width() - totalWidth - (This.children.length -1) * This.spacing)/2;
    jQuery.each(This.children, function (k,e) {
        console.log(e.options.hoffset, e.options.voffset);
        $(e.child).css({position: 'absolute', 'left': x + e.options.hoffset, 'top': ($(This.parent).height() - $(e.child).height())/2 + e.options.voffset}  );
        x = x + $(e.child).width() + This.spacing
    });
};

